import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'reactstrap';
import BodyContent from './components/body/bodyContent';
import HeaderContent from './components/header/headerContent';

function App() {
  return (
    <Container className='text-center'>
      <HeaderContent/>
      <BodyContent/>        
    </Container>
  );
}

export default App;
