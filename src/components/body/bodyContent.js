import { useState } from "react";
import InputBody from "./input-body/inputBody";
import OutputBody from "./output-body/outputBody";


function BodyContent() {
    const [inputMessage, setInputMessage] = useState("");
    const [outputMessage, setOutputMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState(false);

    //hàm cho phép thay đổi giá trị input message
    const inputMessageChangeHandler = (value) => {
        console.log("thay doi gia tri inputmessage: " + value);
        setInputMessage(value);
    }

    //hàm cho phép thay đổi giá trị output message
    const outputMessageChangeHandler = () => {
        if (inputMessage) {
            setOutputMessage([...outputMessage, inputMessage]);
            setLikeDisplay(true);
        }
    }

    return (
        <>
            <InputBody inputMessageProps={inputMessage} inputMessageChangeHandlerProp={inputMessageChangeHandler} outputMessageChangeHandlerProp={outputMessageChangeHandler}/>
            <OutputBody outputMessageProps={outputMessage} likeDisplayProps={likeDisplay} />
        </>
    )
}

export default BodyContent