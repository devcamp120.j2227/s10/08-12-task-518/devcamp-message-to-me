import ImageHeader from "./image-header/imageHeader";
import TextHeader from "./text-header/textHeader";

function HeaderContent() {
    return (
        <>
            <TextHeader/>
            <ImageHeader/>
        </>
    )
}

export default HeaderContent;